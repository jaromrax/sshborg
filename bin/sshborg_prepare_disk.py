#!/usr/bin/env  python3

from fire import Fire
import os
import sys
from pathlib import Path



def get_local_id_rsa_pub():
    pub = os.path.expanduser("~/.ssh/id_rsa.pub")
    with open(pub,"r") as f:
        key =f.readlines()
    print("i... key==",key)
    if len(key)!=1:
        print(f"!... LOCAL KEY PROBLEM {key}")
        sys.exit()
    return key[0]



def get_size(filename):
    if os.path.isfile( filename ):
        st = os.stat(filename)
        return st.st_size
    else:
        print("!... no file", filename)
        sys.exit()


def mkdir_in_mount( mountpoint, newdir ):
    """
    create subdirectory in mountpoint
    """
    # newdir = ".ssh"
    print("i... creating {newdir}")
    Path(f"{mountpoint}/{newdir}").mkdir(parents=True, exist_ok=True)



def main( mountpoint , replace_auth = False, docker_show=False, ):
    """
    MAIN function
    """
    print(" THIS IS A DISK PREPARATION FOR SSHBORG")
    print(" ---------------------------------------")
    print(" ")
    print("  ")

    mountpoint = mountpoint.rstrip("/")

    print("i...  mountpoint is" ,mountpoint )
    if os.path.isdir( mountpoint ):
        print("i... OK")
    else:
        print("!... NOT OK")
        sys.exit()

    if not docker_show:
        mkdir_in_mount(mountpoint, "borgssh")
        mkdir_in_mount(mountpoint, "borgssh/borg")

    # mkdir_in_mount(mountpoint, "borgssh/.ssh")
    if docker_show:
        print(f"""

i...   initiate the borg docker....

 docker run -d  --name=openssh-server-borg   --hostname=openssh-server-borg  -e  PUID=1000   -e PGID=1000   -e TZ=Europe/London     -e SUDO_ACCESS=true   -e PASSWORD_ACCESS=false   -e USER_PASSWORD=pas   -e USER_NAME=borg   -p 2222:2222   -v {mountpoint}/borgssh:/config   --restart unless-stopped   openssh-server-borg


""")
        sys.exit()

    si = get_size( f"{mountpoint}/borgssh/.ssh/authorized_keys")

    print(f"i... size of {mountpoint}/borgssh/.ssh/authorized_keys == {si}")
    if (si==0) and (replace_auth):
        localkey = get_local_id_rsa_pub()
        print(f"i... WRITING TO {mountpoint}/borgssh/.ssh/authorized_keys")
        with open(f"{mountpoint}/borgssh/.ssh/authorized_keys","a") as f:
            newkeys = f'''command="/usr/bin/borg serve --restrict-to-path /config/borg" ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDHAVoMNcW+y4cpmSECC/8RD6YBAdrT7Lh9L8d5z+E5I1p5ZDqJPRpjHnalXFejtSG6/4x79SMk8V58Jpe90d2ca02pTKzsCnZ35LalfJFRnCD/d0Ps8X0dQr7Vs/9stKplQLw4ijshAbwyDVULAki4LDe2IEy6/LIJIhqu7aS/7IYkZwYY7XK834bJLAE2HHoYZ6zTDTF7b3XdNfy8Am/7ymDZvNBTWmklMlwCgUYnxmyVHgdsrztERU+TZdixqP3rHLTdKL9EZnjMcwNl0DANi6gr74K/8DC2PzGvPtCTEQQXiBq8qkG0ZQZ7cmkii2F/jRfNCUfpIf+np4IgirM8y7wUuWbItSNL7l7i6LAsTQNXSZIsQEE2R6CVnym2+qBR+WQX7Vdbe1sFiYvqyD6mdDVvFQoFb2SBNZgxO2fsPk0jjUBXBS4N2aIWHmmRSZY/2zgdG//wYDoHuzrf7rMn9jNXbLdBqagnHlL2As5RDo1UHj41fbhPFVy95kPBoSU= ojr@zen
command="/usr/bin/borg serve --restrict-to-path /config/borg" {localkey}
'''
            f.write(newkeys)
            print(newkeys)
    else:
        print("!... not writing to authorized_keys")
        sys.exit()

    print(f"i... in {mountpoint}/borgssh/borg/ - do borg init . -e none")

#     print("""
# i... test with ssh borg@127.0.0.1 -p 2222
# and install borgbackup inside.
# ```
# ssh borg@127.0.0.1 -p 2222
# sudo borg
# apk update && apk add borgbackup --upgrade
# ```

# """ )

"""

# create keys
ssh-keygen -A

# create sshd_config
AuthorizedKeysFile      .ssh/authorized_keys
PasswordAuthentication no
AllowTcpForwarding no
GatewayPorts no
X11Forwarding no
PidFile /config/sshd.pid
Subsystem       sftp    /usr/lib/ssh/sftp-server -u 022

# run
/sbin/sshd -D -e -p 2222

"""


if __name__ == "__main__":
    # sudo docker ps |  grep openssh-server-borg | cut -d " " -f 1

    print("_"*50)
    print("""
    1. make sure no openssh-server-borg  docker is running...
    2. run [this]  MOUNTPOINT     ... it creates the basic directory
    3. run [this]  MOUNTPOINT -d  ... run the docker as adviced
    4. run [this]  MOUNTPOINT -r  ... to replace .ssh/authorized_keys
    5. docker exec -it f428f8138bdf bash
       001  cd /config/borg/
       002  borg init . -e none
       102  chown -R  borg:users borg/
       108  apk add install openssh
       109  apk add install open-ssh
       126  cd /config/ssh_host_keys/
echo "# create sshd_config
AuthorizedKeysFile      .ssh/authorized_keys
PasswordAuthentication no
AllowTcpForwarding no
GatewayPorts no
X11Forwarding no
PidFile /config/sshd.pid
Subsystem       sftp    /usr/lib/ssh/sftp-server -u 022
" >  sshd_config
       130  ssh-keygen -A
       133  nohup /usr/sbin/sshd -D -e -p 2222 &


""")
    print("_"*50)
    Fire( main )
